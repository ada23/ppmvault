pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;

package libkern_i386_uOSByteOrder_h is

   --  skipped func _OSSwapInt16

   --  skipped func _OSSwapInt32

   --  skipped func _OSSwapInt64

end libkern_i386_uOSByteOrder_h;

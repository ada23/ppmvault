pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with sodium_randombytes_h;

package sodium_randombytes_internal_random_h is

   --  unsupported macro: randombytes_salsa20_implementation randombytes_internal_implementation
   randombytes_internal_implementation : aliased sodium_randombytes_h.randombytes_implementation  -- /Users/rajasrinivasan/include/sodium/randombytes_internal_random.h:13
   with Import => True, 
        Convention => C, 
        External_Name => "randombytes_internal_implementation";

end sodium_randombytes_internal_random_h;

pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;
with System;
with stddef_h;
with Interfaces.C.Extensions;
with utypes_uuint32_t_h;
with sys_utypes_udev_t_h;
with sys_utypes_umode_t_h;

package stdlib_h is

   EXIT_FAILURE : constant := 1;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:102
   EXIT_SUCCESS : constant := 0;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:103

   RAND_MAX : constant := 16#7fffffff#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:105
   --  unsupported macro: MB_CUR_MAX __mb_cur_max

   --  skipped anonymous struct anon_anon_5

   type div_t is record
      quot : aliased int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:84
      c_rem : aliased int;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:85
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:86

   --  skipped anonymous struct anon_anon_6

   type ldiv_t is record
      quot : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:89
      c_rem : aliased long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:90
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:91

   --  skipped anonymous struct anon_anon_7

   type lldiv_t is record
      quot : aliased Long_Long_Integer;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:95
      c_rem : aliased Long_Long_Integer;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:96
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:97

   procedure c_abort  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:131
   with Import => True, 
        Convention => C, 
        External_Name => "abort";

   function c_abs (arg1 : int) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:132
   with Import => True, 
        Convention => C, 
        External_Name => "abs";

   function atexit (arg1 : access procedure) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:133
   with Import => True, 
        Convention => C, 
        External_Name => "atexit";

   function atof (arg1 : Interfaces.C.Strings.chars_ptr) return double  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:134
   with Import => True, 
        Convention => C, 
        External_Name => "atof";

   function atoi (arg1 : Interfaces.C.Strings.chars_ptr) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:135
   with Import => True, 
        Convention => C, 
        External_Name => "atoi";

   function atol (arg1 : Interfaces.C.Strings.chars_ptr) return long  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:136
   with Import => True, 
        Convention => C, 
        External_Name => "atol";

   function atoll (arg1 : Interfaces.C.Strings.chars_ptr) return Long_Long_Integer  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:139
   with Import => True, 
        Convention => C, 
        External_Name => "atoll";

   function bsearch
     (uu_key : System.Address;
      uu_base : System.Address;
      uu_nel : stddef_h.size_t;
      uu_width : stddef_h.size_t;
      uu_compar : access function (arg1 : System.Address; arg2 : System.Address) return int) return System.Address  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:141
   with Import => True, 
        Convention => C, 
        External_Name => "bsearch";

   function div (arg1 : int; arg2 : int) return div_t  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:144
   with Import => True, 
        Convention => C, 
        External_Name => "div";

   procedure c_exit (arg1 : int)  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:145
   with Import => True, 
        Convention => C, 
        External_Name => "exit";

   function getenv (arg1 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:147
   with Import => True, 
        Convention => C, 
        External_Name => "getenv";

   function labs (arg1 : long) return long  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:148
   with Import => True, 
        Convention => C, 
        External_Name => "labs";

   function ldiv (arg1 : long; arg2 : long) return ldiv_t  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:149
   with Import => True, 
        Convention => C, 
        External_Name => "ldiv";

   function llabs (arg1 : Long_Long_Integer) return Long_Long_Integer  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:152
   with Import => True, 
        Convention => C, 
        External_Name => "llabs";

   function lldiv (arg1 : Long_Long_Integer; arg2 : Long_Long_Integer) return lldiv_t  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:153
   with Import => True, 
        Convention => C, 
        External_Name => "lldiv";

   function mblen (uu_s : Interfaces.C.Strings.chars_ptr; uu_n : stddef_h.size_t) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:156
   with Import => True, 
        Convention => C, 
        External_Name => "mblen";

   function mbstowcs
     (arg1 : access wchar_t;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return stddef_h.size_t  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:157
   with Import => True, 
        Convention => C, 
        External_Name => "mbstowcs";

   function mbtowc
     (arg1 : access wchar_t;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:158
   with Import => True, 
        Convention => C, 
        External_Name => "mbtowc";

   procedure qsort
     (uu_base : System.Address;
      uu_nel : stddef_h.size_t;
      uu_width : stddef_h.size_t;
      uu_compar : access function (arg1 : System.Address; arg2 : System.Address) return int)  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:160
   with Import => True, 
        Convention => C, 
        External_Name => "qsort";

   function rand return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:162
   with Import => True, 
        Convention => C, 
        External_Name => "rand";

   procedure srand (arg1 : unsigned)  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:164
   with Import => True, 
        Convention => C, 
        External_Name => "srand";

   function strtod (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : System.Address) return double  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:165
   with Import => True, 
        Convention => C, 
        External_Name => "_strtod";

   function strtof (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : System.Address) return float  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:166
   with Import => True, 
        Convention => C, 
        External_Name => "_strtof";

   function strtol
     (uu_str : Interfaces.C.Strings.chars_ptr;
      uu_endptr : System.Address;
      uu_base : int) return long  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:167
   with Import => True, 
        Convention => C, 
        External_Name => "strtol";

   function strtold (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : System.Address) return long_double  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:169
   with Import => True, 
        Convention => C, 
        External_Name => "strtold";

   function strtoll
     (uu_str : Interfaces.C.Strings.chars_ptr;
      uu_endptr : System.Address;
      uu_base : int) return Long_Long_Integer  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:172
   with Import => True, 
        Convention => C, 
        External_Name => "strtoll";

   function strtoul
     (uu_str : Interfaces.C.Strings.chars_ptr;
      uu_endptr : System.Address;
      uu_base : int) return unsigned_long  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:175
   with Import => True, 
        Convention => C, 
        External_Name => "strtoul";

   function strtoull
     (uu_str : Interfaces.C.Strings.chars_ptr;
      uu_endptr : System.Address;
      uu_base : int) return Extensions.unsigned_long_long  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:178
   with Import => True, 
        Convention => C, 
        External_Name => "strtoull";

   function c_system (arg1 : Interfaces.C.Strings.chars_ptr) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:184
   with Import => True, 
        Convention => C, 
        External_Name => "_system";

   function wcstombs
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : access wchar_t;
      arg3 : stddef_h.size_t) return stddef_h.size_t  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:187
   with Import => True, 
        Convention => C, 
        External_Name => "wcstombs";

   function wctomb (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : wchar_t) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:188
   with Import => True, 
        Convention => C, 
        External_Name => "wctomb";

   --  skipped func _Exit

   function a64l (arg1 : Interfaces.C.Strings.chars_ptr) return long  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:192
   with Import => True, 
        Convention => C, 
        External_Name => "a64l";

   function drand48 return double  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:193
   with Import => True, 
        Convention => C, 
        External_Name => "drand48";

   function ecvt
     (arg1 : double;
      arg2 : int;
      arg3 : access int;
      arg4 : access int) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:194
   with Import => True, 
        Convention => C, 
        External_Name => "ecvt";

   function erand48 (arg1 : access unsigned_short) return double  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:195
   with Import => True, 
        Convention => C, 
        External_Name => "erand48";

   function fcvt
     (arg1 : double;
      arg2 : int;
      arg3 : access int;
      arg4 : access int) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:196
   with Import => True, 
        Convention => C, 
        External_Name => "fcvt";

   function gcvt
     (arg1 : double;
      arg2 : int;
      arg3 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:197
   with Import => True, 
        Convention => C, 
        External_Name => "gcvt";

   function getsubopt
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : System.Address) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:198
   with Import => True, 
        Convention => C, 
        External_Name => "getsubopt";

   function grantpt (arg1 : int) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:199
   with Import => True, 
        Convention => C, 
        External_Name => "grantpt";

   function initstate
     (arg1 : unsigned;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : stddef_h.size_t) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:201
   with Import => True, 
        Convention => C, 
        External_Name => "initstate";

   function jrand48 (arg1 : access unsigned_short) return long  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:205
   with Import => True, 
        Convention => C, 
        External_Name => "jrand48";

   function l64a (arg1 : long) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:206
   with Import => True, 
        Convention => C, 
        External_Name => "l64a";

   procedure lcong48 (arg1 : access unsigned_short)  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:207
   with Import => True, 
        Convention => C, 
        External_Name => "lcong48";

   function lrand48 return long  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:208
   with Import => True, 
        Convention => C, 
        External_Name => "lrand48";

   function mktemp (arg1 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:209
   with Import => True, 
        Convention => C, 
        External_Name => "mktemp";

   function mkstemp (arg1 : Interfaces.C.Strings.chars_ptr) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:210
   with Import => True, 
        Convention => C, 
        External_Name => "mkstemp";

   function mrand48 return long  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:211
   with Import => True, 
        Convention => C, 
        External_Name => "mrand48";

   function nrand48 (arg1 : access unsigned_short) return long  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:212
   with Import => True, 
        Convention => C, 
        External_Name => "nrand48";

   function posix_openpt (arg1 : int) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:213
   with Import => True, 
        Convention => C, 
        External_Name => "posix_openpt";

   function ptsname (arg1 : int) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:214
   with Import => True, 
        Convention => C, 
        External_Name => "ptsname";

   function ptsname_r
     (fildes : int;
      buffer : Interfaces.C.Strings.chars_ptr;
      buflen : stddef_h.size_t) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:217
   with Import => True, 
        Convention => C, 
        External_Name => "ptsname_r";

   function putenv (arg1 : Interfaces.C.Strings.chars_ptr) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:220
   with Import => True, 
        Convention => C, 
        External_Name => "_putenv";

   function random return long  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:221
   with Import => True, 
        Convention => C, 
        External_Name => "random";

   function rand_r (arg1 : access unsigned) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:222
   with Import => True, 
        Convention => C, 
        External_Name => "rand_r";

   function realpath (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:224
   with Import => True, 
        Convention => C, 
        External_Name => "_realpath$DARWIN_EXTSN";

   function seed48 (arg1 : access unsigned_short) return access unsigned_short  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:229
   with Import => True, 
        Convention => C, 
        External_Name => "seed48";

   function setenv
     (uu_name : Interfaces.C.Strings.chars_ptr;
      uu_value : Interfaces.C.Strings.chars_ptr;
      uu_overwrite : int) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:230
   with Import => True, 
        Convention => C, 
        External_Name => "_setenv";

   procedure setkey (arg1 : Interfaces.C.Strings.chars_ptr)  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:232
   with Import => True, 
        Convention => C, 
        External_Name => "_setkey";

   function setstate (arg1 : Interfaces.C.Strings.chars_ptr) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:236
   with Import => True, 
        Convention => C, 
        External_Name => "setstate";

   procedure srand48 (arg1 : long)  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:237
   with Import => True, 
        Convention => C, 
        External_Name => "srand48";

   procedure srandom (arg1 : unsigned)  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:239
   with Import => True, 
        Convention => C, 
        External_Name => "srandom";

   function unlockpt (arg1 : int) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:243
   with Import => True, 
        Convention => C, 
        External_Name => "unlockpt";

   function unsetenv (arg1 : Interfaces.C.Strings.chars_ptr) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:245
   with Import => True, 
        Convention => C, 
        External_Name => "_unsetenv";

   function arc4random return utypes_uuint32_t_h.uint32_t  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:257
   with Import => True, 
        Convention => C, 
        External_Name => "arc4random";

   procedure arc4random_addrandom (arg1 : access unsigned_char; arg2 : int)  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:258
   with Import => True, 
        Convention => C, 
        External_Name => "arc4random_addrandom";

   procedure arc4random_buf (uu_buf : System.Address; uu_nbytes : stddef_h.size_t)  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:263
   with Import => True, 
        Convention => C, 
        External_Name => "arc4random_buf";

   procedure arc4random_stir  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:264
   with Import => True, 
        Convention => C, 
        External_Name => "arc4random_stir";

   function arc4random_uniform (uu_upper_bound : utypes_uuint32_t_h.uint32_t) return utypes_uuint32_t_h.uint32_t  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:266
   with Import => True, 
        Convention => C, 
        External_Name => "arc4random_uniform";

   function cgetcap
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : int) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:283
   with Import => True, 
        Convention => C, 
        External_Name => "cgetcap";

   function cgetclose return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:284
   with Import => True, 
        Convention => C, 
        External_Name => "cgetclose";

   function cgetent
     (arg1 : System.Address;
      arg2 : System.Address;
      arg3 : Interfaces.C.Strings.chars_ptr) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:285
   with Import => True, 
        Convention => C, 
        External_Name => "cgetent";

   function cgetfirst (arg1 : System.Address; arg2 : System.Address) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:286
   with Import => True, 
        Convention => C, 
        External_Name => "cgetfirst";

   function cgetmatch (arg1 : Interfaces.C.Strings.chars_ptr; arg2 : Interfaces.C.Strings.chars_ptr) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:287
   with Import => True, 
        Convention => C, 
        External_Name => "cgetmatch";

   function cgetnext (arg1 : System.Address; arg2 : System.Address) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:288
   with Import => True, 
        Convention => C, 
        External_Name => "cgetnext";

   function cgetnum
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : access long) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:289
   with Import => True, 
        Convention => C, 
        External_Name => "cgetnum";

   function cgetset (arg1 : Interfaces.C.Strings.chars_ptr) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:290
   with Import => True, 
        Convention => C, 
        External_Name => "cgetset";

   function cgetstr
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : System.Address) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:291
   with Import => True, 
        Convention => C, 
        External_Name => "cgetstr";

   function cgetustr
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : System.Address) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:292
   with Import => True, 
        Convention => C, 
        External_Name => "cgetustr";

   function daemon (arg1 : int; arg2 : int) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:294
   with Import => True, 
        Convention => C, 
        External_Name => "_daemon$1050";

   function devname (arg1 : sys_utypes_udev_t_h.dev_t; arg2 : sys_utypes_umode_t_h.mode_t) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:295
   with Import => True, 
        Convention => C, 
        External_Name => "devname";

   function devname_r
     (arg1 : sys_utypes_udev_t_h.dev_t;
      arg2 : sys_utypes_umode_t_h.mode_t;
      buf : Interfaces.C.Strings.chars_ptr;
      len : int) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:296
   with Import => True, 
        Convention => C, 
        External_Name => "devname_r";

   function getbsize (arg1 : access int; arg2 : access long) return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:297
   with Import => True, 
        Convention => C, 
        External_Name => "getbsize";

   function getloadavg (arg1 : access double; arg2 : int) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:298
   with Import => True, 
        Convention => C, 
        External_Name => "getloadavg";

   function getprogname return Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:300
   with Import => True, 
        Convention => C, 
        External_Name => "getprogname";

   procedure setprogname (arg1 : Interfaces.C.Strings.chars_ptr)  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:301
   with Import => True, 
        Convention => C, 
        External_Name => "setprogname";

   function heapsort
     (uu_base : System.Address;
      uu_nel : stddef_h.size_t;
      uu_width : stddef_h.size_t;
      uu_compar : access function (arg1 : System.Address; arg2 : System.Address) return int) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:311
   with Import => True, 
        Convention => C, 
        External_Name => "heapsort";

   function mergesort
     (uu_base : System.Address;
      uu_nel : stddef_h.size_t;
      uu_width : stddef_h.size_t;
      uu_compar : access function (arg1 : System.Address; arg2 : System.Address) return int) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:318
   with Import => True, 
        Convention => C, 
        External_Name => "mergesort";

   procedure psort
     (uu_base : System.Address;
      uu_nel : stddef_h.size_t;
      uu_width : stddef_h.size_t;
      uu_compar : access function (arg1 : System.Address; arg2 : System.Address) return int)  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:325
   with Import => True, 
        Convention => C, 
        External_Name => "psort";

   procedure psort_r
     (uu_base : System.Address;
      uu_nel : stddef_h.size_t;
      uu_width : stddef_h.size_t;
      arg4 : System.Address;
      uu_compar : access function
        (arg1 : System.Address;
         arg2 : System.Address;
         arg3 : System.Address) return int)  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:333
   with Import => True, 
        Convention => C, 
        External_Name => "psort_r";

   procedure qsort_r
     (uu_base : System.Address;
      uu_nel : stddef_h.size_t;
      uu_width : stddef_h.size_t;
      arg4 : System.Address;
      uu_compar : access function
        (arg1 : System.Address;
         arg2 : System.Address;
         arg3 : System.Address) return int)  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:341
   with Import => True, 
        Convention => C, 
        External_Name => "qsort_r";

   function radixsort
     (uu_base : System.Address;
      uu_nel : int;
      uu_table : access unsigned_char;
      uu_endbyte : unsigned) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:343
   with Import => True, 
        Convention => C, 
        External_Name => "radixsort";

   function rpmatch (arg1 : Interfaces.C.Strings.chars_ptr) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:345
   with Import => True, 
        Convention => C, 
        External_Name => "rpmatch";

   function sradixsort
     (uu_base : System.Address;
      uu_nel : int;
      uu_table : access unsigned_char;
      uu_endbyte : unsigned) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:347
   with Import => True, 
        Convention => C, 
        External_Name => "sradixsort";

   procedure sranddev  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:349
   with Import => True, 
        Convention => C, 
        External_Name => "sranddev";

   procedure srandomdev  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:350
   with Import => True, 
        Convention => C, 
        External_Name => "srandomdev";

   function reallocf (uu_ptr : System.Address; uu_size : stddef_h.size_t) return System.Address  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:351
   with Import => True, 
        Convention => C, 
        External_Name => "reallocf";

   function strtonum
     (uu_numstr : Interfaces.C.Strings.chars_ptr;
      uu_minval : Long_Long_Integer;
      uu_maxval : Long_Long_Integer;
      uu_errstrp : System.Address) return Long_Long_Integer  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:353
   with Import => True, 
        Convention => C, 
        External_Name => "strtonum";

   function strtoq
     (uu_str : Interfaces.C.Strings.chars_ptr;
      uu_endptr : System.Address;
      uu_base : int) return Long_Long_Integer  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:357
   with Import => True, 
        Convention => C, 
        External_Name => "strtoq";

   function strtouq
     (uu_str : Interfaces.C.Strings.chars_ptr;
      uu_endptr : System.Address;
      uu_base : int) return Extensions.unsigned_long_long  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:359
   with Import => True, 
        Convention => C, 
        External_Name => "strtouq";

   suboptarg : Interfaces.C.Strings.chars_ptr  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/stdlib.h:361
   with Import => True, 
        Convention => C, 
        External_Name => "suboptarg";

end stdlib_h;

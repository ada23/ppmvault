pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;

package sodium_core_h is

   function sodium_init return int  -- /Users/rajasrinivasan/include/sodium/core.h:12
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_init";

   function sodium_set_misuse_handler (handler : access procedure) return int  -- /Users/rajasrinivasan/include/sodium/core.h:18
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_set_misuse_handler";

   procedure sodium_misuse  -- /Users/rajasrinivasan/include/sodium/core.h:21
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_misuse";

end sodium_core_h;

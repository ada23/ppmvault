pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;

package sys_utypes_uu_int16_t_h is

   subtype u_int16_t is unsigned_short;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_u_int16_t.h:30

end sys_utypes_uu_int16_t_h;

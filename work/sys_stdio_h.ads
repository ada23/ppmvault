pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;

package sys_stdio_h is

   RENAME_SECLUDE : constant := 16#00000001#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/stdio.h:43
   RENAME_SWAP : constant := 16#00000002#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/stdio.h:44
   RENAME_EXCL : constant := 16#00000004#;  --  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/stdio.h:45

   function renameat
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : int;
      arg4 : Interfaces.C.Strings.chars_ptr) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/stdio.h:39
   with Import => True, 
        Convention => C, 
        External_Name => "renameat";

   function renamex_np
     (arg1 : Interfaces.C.Strings.chars_ptr;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : unsigned) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/stdio.h:46
   with Import => True, 
        Convention => C, 
        External_Name => "renamex_np";

   function renameatx_np
     (arg1 : int;
      arg2 : Interfaces.C.Strings.chars_ptr;
      arg3 : int;
      arg4 : Interfaces.C.Strings.chars_ptr;
      arg5 : unsigned) return int  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/stdio.h:47
   with Import => True, 
        Convention => C, 
        External_Name => "renameatx_np";

end sys_stdio_h;

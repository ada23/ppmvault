pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with sys_utypes_uu_int64_t_h;
with sys_utypes_uint64_t_h;
with sys_utypes_uint32_t_h;
with sys_utypes_uu_int32_t_h;
with i386_utypes_h;
with sys_utypes_udev_t_h;

package sys_types_h is

   --  unsupported macro: NBBY __DARWIN_NBBY
   --  unsupported macro: NFDBITS __DARWIN_NFDBITS
   --  arg-macro: procedure howmany (x, y)
   --    __DARWIN_howmany(x, y)
   subtype u_long is unsigned_long;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:88

   subtype ushort is unsigned_short;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:91

   subtype uint is unsigned;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:92

   subtype u_quad_t is sys_utypes_uu_int64_t_h.u_int64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:95

   subtype quad_t is sys_utypes_uint64_t_h.int64_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:96

   type qaddr_t is access all quad_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:97

   subtype daddr_t is sys_utypes_uint32_t_h.int32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:101

   subtype fixpt_t is sys_utypes_uu_int32_t_h.u_int32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:105

   subtype segsz_t is sys_utypes_uint32_t_h.int32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:125

   subtype swblk_t is sys_utypes_uint32_t_h.int32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:126

   function major (u_x : i386_utypes_h.uu_uint32_t) return i386_utypes_h.uu_int32_t  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:139
   with Import => True, 
        Convention => C, 
        External_Name => "major";

   function minor (u_x : i386_utypes_h.uu_uint32_t) return i386_utypes_h.uu_int32_t  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:145
   with Import => True, 
        Convention => C, 
        External_Name => "minor";

   function makedev (u_major : i386_utypes_h.uu_uint32_t; u_minor : i386_utypes_h.uu_uint32_t) return sys_utypes_udev_t_h.dev_t  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:151
   with Import => True, 
        Convention => C, 
        External_Name => "makedev";

   subtype fd_mask is i386_utypes_h.uu_int32_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/types.h:189

end sys_types_h;

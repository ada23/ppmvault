pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with i386_utypes_h;
with sys_utypes_h;

package sys_utypes_utimeval_h is

   type timeval is record
      tv_sec : aliased i386_utypes_h.uu_darwin_time_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_timeval.h:36
      tv_usec : aliased sys_utypes_h.uu_darwin_suseconds_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_timeval.h:37
   end record
   with Convention => C_Pass_By_Copy;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_timeval.h:34

end sys_utypes_utimeval_h;

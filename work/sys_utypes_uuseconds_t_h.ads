pragma Ada_2012;
pragma Style_Checks (Off);
pragma Warnings ("U");

with Interfaces.C; use Interfaces.C;
with sys_utypes_h;

package sys_utypes_uuseconds_t_h is

   subtype useconds_t is sys_utypes_h.uu_darwin_useconds_t;  -- /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/include/sys/_types/_useconds_t.h:31

end sys_utypes_uuseconds_t_h;

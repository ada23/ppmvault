-------------------------------------------------------------------------------
-- Generic hashing
-- https://doc.libsodium.org/hashing/generic_hashing
-------------------------------------------------------------------------------
with System ; use System ;
with interfaces.C ; use Interfaces.C ;

package sodium.hash is
   
   type State_Ptr is new Address ;
   
   type Hash_Ptr is new Block_Ptr ;
   type Key_Ptr is new Block_Ptr ;
   
   function Hash( c_in : Address ;
                  inlen : integer ;
                  Key : Address := Null_Address ;
                  keylen : integer := 0 ) return Hash_Ptr ;
   function Hash( c_in : Address ;
                  inlen : integer ;
                  Key : Address := Null_Address ;
                  keylen : integer := 0 ) return String ;
   function Hash( c_in : Address ;
                  inlen : integer ;
                  key : Key_Ptr ) return String ;
   
   function Create return Key_Ptr ;
   
   function Create return State_Ptr ;
   function Create( key : Key_Ptr ) return State_Ptr ;
   
   procedure InitState( state : State_Ptr ; key : Key_Ptr := null );
   procedure Update( state : State_Ptr ;
                     c_in : Address ;
                     inlen : integer ) ;
   function Final( state : State_Ptr ) return Hash_Ptr ;
   function Final( state : State_Ptr ) return String ;
   
   function Digest( filename : string ; key : Key_Ptr := null ) return String ;
   
   -- Short-input hashing
   -- https://doc.libsodium.org/hashing/short-input_hashing

   subtype ShortHash_Type is unsigned_long_long ;
   type ShortHashKey_Ptr is new Block_Ptr ;

   function ShortHash( c_in : Address ;
                       inlen : unsigned_long_long;
                       k : ShortHashKey_Ptr ) return ShortHash_Type ;
   
   function ShortHashKeyGen(random : boolean := true ) return ShortHashKey_Ptr ;
end sodium.hash;

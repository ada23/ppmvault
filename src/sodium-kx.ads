-- Key exchange
-- https://doc.libsodium.org/key_exchange
with Interfaces ; use Interfaces ;
package sodium.kx is

   type Public_KeyPtr is new Block_Ptr ;
   type Secret_KeyPtr is new Block_Ptr ;
   type Session_KeyPtr is new Block_Ptr ;
   type Seed_Ptr is new Block_Ptr ;
   
   function Create return Public_KeyPtr ;
   function Create return Secret_KeyPtr ;
   
   function Value( b64str : String ) return Public_KeyPtr ;
   function Image( key : Public_KeyPtr ) return String ;
   function Value( b64str : String ) return Secret_KeyPtr ;
   function Image( key : Secret_KeyPtr ) return String ;
   
   function Create (init : Unsigned_8 := 16#1f# ) return Seed_Ptr ;
   function Create return Session_KeyPtr ;
   
   procedure Generate( pk : out Public_KeyPtr ; sk : out Secret_KeyPtr );
   procedure Generate( pk : out Public_KeyPtr ; sk : out Secret_KeyPtr ;
                       seed : Seed_Ptr );
   procedure Generate( rx : out Session_KeyPtr ;
                       tx : out Session_KeyPtr ;
                       client_pk : Public_KeyPtr ;
                       client_sk : Secret_KeyPtr ;
                       server_pk : Public_KeyPtr ) ;
   procedure GenerateS( rx : out Session_KeyPtr ;
                       tx : out Session_KeyPtr ;
                       server_pk : Public_KeyPtr ;
                       server_sk : Secret_KeyPtr ;
                       client_pk : Public_KeyPtr ) ;
   
end sodium.kx;

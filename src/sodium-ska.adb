with System; use System ;
with Interfaces.C ; use Interfaces.C ;

package body sodium.ska is

   
   function crypto_auth_bytes return size_t  -- ../include/sodium/crypto_auth.h:18
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_auth_bytes";

   function crypto_auth_keybytes return size_t  -- ../include/sodium/crypto_auth.h:22
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_auth_keybytes";

   
   function crypto_auth
     (c_out : Address;
      c_in : Address;
      inlen : unsigned_long_long;
      k : Address) return int  -- ../include/sodium/crypto_auth.h:29
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_auth";

   function crypto_auth_verify
     (h : Address;
      c_in : Address;
      inlen : unsigned_long_long;
      k : Address) return int  -- ../include/sodium/crypto_auth.h:34
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_auth_verify";

   procedure crypto_auth_keygen (k : Address)  -- ../include/sodium/crypto_auth.h:39
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_auth_keygen";

   
   function Create return KeyPtr_Type is
      result : KeyPtr_Type ;
   begin
      result := KeyPtr_Type( sodium.Create(crypto_auth_keybytes) ) ;
      crypto_auth_keygen( result.all'Address ) ;
      return result ;
   end Create ;
   function Generate( m : Address ; mlen : integer ; k : KeyPtr_Type ) return MAC_Ptr is
      result : MAC_Ptr ;
      status : int ;
   begin
      result := MAC_Ptr(sodium.Create( crypto_auth_bytes )) ;
      status := crypto_auth( result.all'Address ,
                             m , unsigned_long_long(mlen) ,
                             k.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_auth" ;
      end if ;
      return result ;
   end Generate ;
   function Verify( h : MAC_Ptr ; m : Address ; mlen : integer ; K : KeyPtr_Type ) return boolean is
      status : int ;
   begin
      status := crypto_auth_verify( h.all'Address , m , unsigned_long_long(mlen) , k.all'Address ) ;
      if status = 0
      then
         return true ;
      end if ;
      return false ;
   end Verify ;
   
   function Verify( h : Address ; m : Address ; mlen : integer ; K : KeyPtr_Type ) return boolean is
      status : int ;
   begin
      status := crypto_auth_verify( h , m , unsigned_long_long(mlen) , k.all'Address ) ;
      if status = 0
      then
         return true ;
      end if ;
      return false ;
   end Verify ;
   
   
end sodium.ska;

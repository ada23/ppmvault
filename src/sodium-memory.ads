with System; use System;
with Interfaces.C ; use Interfaces.C ;

package sodium.memory is
   
   function malloc (size : size_t) return System.Address  -- ../include/sodium/utils.h:142
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_malloc";
   
   function allocarray (count : size_t; size : size_t) return System.Address  -- ../include/sodium/utils.h:146
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_allocarray";

   procedure free (ptr : System.Address)  -- ../include/sodium/utils.h:150
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_free";
   
end sodium.memory;

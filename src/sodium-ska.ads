------------------------------------------------------------------------------
-- Secret Key Cryptography
-- Authentication
-- https://doc.libsodium.org/secret-key_cryptography/secret-key_authentication
------------------------------------------------------------------------------
with System ; use System ;

package sodium.ska is

   type KeyPtr_Type is new Block_Ptr ;
   type MAC_Ptr is new Block_Ptr ;
   
   function Create return KeyPtr_Type ;
   function Generate( m : Address ; mlen : integer ; k : KeyPtr_Type ) return MAC_Ptr ;
   function Verify( h : MAC_Ptr ; m : Address ; mlen : integer ; K : KeyPtr_Type ) return boolean ;
   function Verify( h : Address ; m : Address ; mlen : integer ; K : KeyPtr_Type ) return boolean ;
  
end sodium.ska;

with Ada.Streams ; use Ada.Streams ;
with Ada.Streams.Stream_Io; use Ada.Streams.Stream_Io;

package body sodium.pks is

   function crypto_sign_bytes return size_t  -- ../include/sodium/crypto_sign.h:30
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_sign_bytes";

   function crypto_sign_seedbytes return size_t  -- ../include/sodium/crypto_sign.h:34
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_sign_seedbytes";

   function crypto_sign_publickeybytes return size_t  -- ../include/sodium/crypto_sign.h:38
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_sign_publickeybytes";

   function crypto_sign_secretkeybytes return size_t  -- ../include/sodium/crypto_sign.h:42
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_sign_secretkeybytes";
   
   function Create( init : Unsigned_8 := 16#86# ) return Seed_Ptr is
      result : Seed_Ptr := Seed_Ptr( sodium.Create(crypto_sign_seedbytes,init) ) ;
   begin
      return result ;
   end Create ;
   
      function crypto_sign_seed_keypair
     (pk : Address;
      sk : Address;
      seed : Address) return int  -- ../include/sodium/crypto_sign.h:53
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_sign_seed_keypair";

   function crypto_sign_keypair (pk : Address; sk : Address) return int  -- ../include/sodium/crypto_sign.h:58
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_sign_keypair";
   
   procedure Generate( pk : out Public_KeyPtr ; sk : out Secret_KeyPtr ) is
      status : int ;
   begin
      pk := Public_KeyPtr(sodium.Create(crypto_sign_publickeybytes)) ;
      sk := Secret_KeyPtr(sodium.Create(crypto_sign_secretkeybytes));
      status := crypto_sign_keypair(pk.all'Address,sk.all'Address);
      if status /= 0
      then
         raise Program_Error with "crypto_sign_seed_keypair" ;
      end if ;
   end Generate ;
   
   procedure Generate( pk : out Public_KeyPtr ; sk : out Secret_KeyPtr ; seed : Seed_Ptr ) is
     status : int ;
   begin
      pk := Public_KeyPtr(sodium.Create(crypto_sign_publickeybytes));
      sk := Secret_KeyPtr(sodium.Create(crypto_sign_secretkeybytes));
      status := crypto_sign_seed_keypair(pk.all'Address,sk.all'Address,seed.all'Address);
      if status /= 0
      then
         raise Program_Error with "crypto_sign_keypair" ;
      end if ;
   end Generate ;
      
   function crypto_sign
     (sm : Address;
      smlen_p : access unsigned_long_long;
      m : Address;
      mlen : unsigned_long_long;
      sk : Address) return int  -- ../include/sodium/crypto_sign.h:62
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_sign";

   function crypto_sign_open
     (m : Address;
      mlen_p : access unsigned_long_long;
      sm : Address;
      smlen : unsigned_long_long;
      pk : Address) return int  -- ../include/sodium/crypto_sign.h:67
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_sign_open";
   
   function Sign( m : Address ; mlen : integer ;
                  sk : Secret_KeyPtr ) return SignedMsg_Ptr is
      result : SignedMsg_Ptr := SignedMsg_Ptr(sodium.Create(size_t(mlen)+crypto_sign_bytes)) ;
      status : int ;
      smlen : aliased unsigned_long_long ;
   begin
      status := crypto_sign(result.all'Address , smlen'access ,
                            m, unsigned_long_long(mlen), 
                            sk.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_sign" ;
      end if;
      return result ;
   end Sign ;
   
   function Open( sm : SignedMsg_Ptr ; pk : Public_KeyPtr ) return Block_Ptr is
      result : Block_Ptr := sodium.Create(size_t(sm.all'Length) - crypto_sign_bytes) ;
      status : int ;
      mlen : aliased unsigned_long_long ;
   begin
      status := crypto_sign_open( result.all'Address , mlen'access ,
                                  sm.all'Address , sm.all'Length, 
                                 pk.all'Address) ;
      if status /= 0
      then
         raise Program_Error with "crypto_sign_open" ;
      end if ;
      return result ;
   end Open ;
   
   function Open( sm : Address ; smlen : integer ; pk : Public_KeyPtr ) return Block_Ptr is
      result : Block_Ptr := sodium.Create(size_t(smlen) - crypto_sign_bytes) ;
      status : int ;
      mlen : aliased unsigned_long_long ;
   begin
      status := crypto_sign_open( result.all'Address , mlen'access ,
                                  sm , unsigned_long_long(smlen) , 
                                 pk.all'Address) ;
      if status /= 0
      then
         raise Program_Error with "crypto_sign_open" ;
      end if ;
      return result ;
   end Open ;      
   
   function crypto_sign_detached
     (sig : Address;
      siglen_p : access unsigned_long_long;
      m : Address;
      mlen : unsigned_long_long;
      sk : Address) return int  -- ../include/sodium/crypto_sign.h:73
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_sign_detached";

   function crypto_sign_verify_detached
     (sig : Address;
      m : Address;
      mlen : unsigned_long_long;
      pk : Address) return int  -- ../include/sodium/crypto_sign.h:78
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_sign_verify_detached";

   
   function Sign( m : Address ; mlen : integer ;
                  sk : Secret_KeyPtr ) return Signature_Ptr is
      result : Signature_Ptr := Signature_Ptr(sodium.Create(crypto_sign_bytes)) ;
      status : int ;
      siglen : aliased unsigned_long_long ;
   begin
      status := crypto_sign_detached(result.all'Address , siglen'access ,
                            m, unsigned_long_long(mlen), 
                            sk.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "crypto_sign_detached" ;
      end if;
      return result ;
   end Sign ;
         
   function Verify( sm : Signature_Ptr ;
                    m : SignedMsg_Ptr ;
                    pk : Public_KeyPtr ) return boolean is
      status : int ;
   begin
      status := crypto_sign_verify_detached( sm.all'Address , 
                                             m.all'Address ,
                                             m.all'Length ,
                                             pk.all'Address) ;
      if status /= 0
      then
         return false ;
      end if ;
      return true ;
   end Verify ;
   
   function Verify( sm : Address ;
                    m : Address ;
                    mlen : integer ;
                    pk : Public_KeyPtr ) return boolean is
      status : int ;
   begin
      status := crypto_sign_verify_detached( sm , 
                                             m ,
                                             unsigned_long_long(mlen) ,
                                             pk.all'Address) ;
      if status /= 0
      then
         return false ;
      end if ;
      return true ;
   end Verify ;
   
   function crypto_sign_statebytes return size_t  -- ../include/sodium/crypto_sign.h:26
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_sign_statebytes";
   
   function crypto_sign_init (state : Address) return int  -- ../include/sodium/crypto_sign.h:85
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_sign_init";

   function crypto_sign_update
     (state : Address ;
      m : Address ;
      mlen : unsigned_long_long) return int  -- ../include/sodium/crypto_sign.h:88
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_sign_update";

   function crypto_sign_final_create
     (state : Address ;
      sig : Address ;
      siglen_p : access unsigned_long_long;
      sk : Address ) return int  -- ../include/sodium/crypto_sign.h:93
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_sign_final_create";

   function crypto_sign_final_verify
     (state : Address ;
      sig : Address ;
      pk : Address ) return int  -- ../include/sodium/crypto_sign.h:99
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_sign_final_verify";
   
   function Create return Sign_State_Ptr is
      result : Sign_State_Ptr := Sign_State_Ptr(sodium.Create(crypto_sign_statebytes) ) ;
   begin
      return result ;
   end Create ;
   procedure Update( state : in out Sign_State_Ptr ;
                     m : Address ; mlen : Integer ) is
      status : int ;
   begin
      status := crypto_sign_update(state.all'Address , m , unsigned_long_long(mlen));
      if status /= 0
      then
         raise Program_Error with "crypto_sign_update" ;
      end if ;
   end Update ;
   procedure Final( state : in out Sign_State_Ptr ;
                    sk : Secret_KeyPtr ;
                    sig : out Signature_Ptr ) is 
      status : int ;
      siglen : aliased unsigned_long_long ;
   begin
      sig := Signature_Ptr(sodium.Create(crypto_sign_bytes));
      siglen := unsigned_long_long(sig.all'Length) ;
      
      status := crypto_sign_final_create(state.all'Address 
                                         , sig.all'Address , siglen'access  
                                         , sk.all'Address) ;
      if status /= 0
      then
         raise Program_Error with "crypto_sign_final_create" ;
      end if ;
   end Final ;
   
   procedure FinalVerify( state : in out Sign_State_Ptr ;
                          sig : Signature_Ptr ;
                          pk : Public_KeyPtr ;
                          verified : out boolean ) is
      status : int ;
   begin
      status := crypto_sign_final_verify(state.all'Address , 
                                         sig.all'Address , pk.all'Address ) ;
      if status /= 0
      then
         verified := false ;
      else
         verified := true ;
      end if ;
   end FinalVerify ;
   
   procedure FinalVerify( state : in out Sign_State_Ptr ;
                          sig : Address ;
                          pk : Public_KeyPtr ;
                          verified : out boolean ) is
      status : int ;
   begin
      status := crypto_sign_final_verify(state.all'Address , sig , pk.all'address ) ;
      if status /= 0
      then
         verified := false ;
      else
         verified := true ;
      end if ;
   end FinalVerify ;
   function Signature( fn : String ; sk : Secret_KeyPtr ) return Signature_Ptr is  
      use Ada.Streams ;
      f : Ada.Streams.Stream_Io.File_Type ;
      buffer : ada.Streams.Stream_Element_Array(1..Ada.Streams.Stream_Element_Offset(crypto_sign_bytes) ) ;
      bufbytes : ada.Streams.Stream_Element_Count ;
      bytes : ada.Streams.Stream_Element_Count := 0 ;

      result : Signature_Ptr ;
      state : Sign_State_Ptr := Create ;
   begin      
      Open(f , ada.streams.Stream_IO.In_File , fn ) ;
      while not ada.Streams.Stream_IO.End_Of_File(f)
      loop
         Read(f,buffer,bufbytes) ;
         Update(state ,buffer'Address,
                Integer(bufbytes)) ;            
         bytes := bytes + bufbytes ;
      end loop ;
      Close(f) ;
      Final(state,sk,result) ;  
      return result ;      
    end Signature ;
   procedure Signature( fn : String ; sk : Secret_KeyPtr; sigfile : string ) is
      sig : Signature_Ptr := Signature(fn,sk) ;
      buffer : ada.Streams.Stream_Element_Array(1..Stream_Element_Offset(crypto_sign_bytes) ) ;
      for buffer'Address use sig.all'Address ;
      use Ada.Streams ;
      f : Ada.Streams.Stream_Io.File_Type ;
   begin
      Create( f , Ada.Streams.Stream_Io.Out_File, sigfile ) ;
      Write( f , buffer );
      Close(f) ;
   end Signature ;
   
   function Verify( fn : string ; sig : Signature_Ptr ; pk : Public_KeyPtr ) return boolean is
     use Ada.Streams ;
      f : Ada.Streams.Stream_Io.File_Type ;
      buffer : ada.Streams.Stream_Element_Array(1..Ada.Streams.Stream_Element_Offset(crypto_sign_bytes) ) ;
      bufbytes : ada.Streams.Stream_Element_Count ;
      bytes : ada.Streams.Stream_Element_Count := 0 ;

      state : Sign_State_Ptr := Create ;
      result : boolean ;
   begin      
      Open(f , ada.streams.Stream_IO.In_File , fn ) ;
      while not ada.Streams.Stream_IO.End_Of_File(f)
      loop
         Read(f,buffer,bufbytes) ;
         Update(state ,buffer'Address,
                Integer(bufbytes)) ;            
         bytes := bytes + bufbytes ;
      end loop ;
      Close(f) ;
      FinalVerify(state,sig.all'Address,pk,result) ;
      return result ;      
   end Verify ;
   
   function Verify( fn : string ; sigfile : String ; pk : Public_KeyPtr ) return boolean is
      sig : Signature_Ptr := Signature_Ptr(sodium.Create(crypto_sign_bytes)) ;
      
      buffer : Stream_Element_Array(1..Stream_Element_Offset(crypto_sign_bytes) ) ;
      for buffer'Address use sig.all'Address ;
      
      buflen : Stream_Element_Count ;
      use Ada.Streams ;
      f : Ada.Streams.Stream_Io.File_Type ;
   begin
      Open( f , Ada.Streams.Stream_Io.In_File, sigfile ) ;
      Read( f , buffer , buflen );
      Close(f) ;
      if buflen /= buffer'Length
      then
         raise Program_Error with "Verify sigfile" ;
      end if ;
      return Verify(fn,sig,pk) ;
   end Verify ;
   
end sodium.pks;

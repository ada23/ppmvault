with Interfaces.C.Strings ;
with Ada.Text_Io ; use Ada.Text_Io ;
with Ada.Integer_Text_IO ; use Ada.Integer_Text_IO ;
with Ada.Strings.Fixed ;
package body sodium.helpers is

   procedure sodium_bin2hex
     (hex : Address ;
      hex_maxlen : size_t;
      bin : Address ;
      bin_len : size_t) -- ../include/sodium/utils.h:60
   with Import => True,
        Convention => C,
     External_Name => "sodium_bin2hex";

   
   function bin2hex(ptr : Address ; len : integer ) return string is
      --result : string (1..2*len+1) ;
      result : char_array(1..size_t(2*len+1)) ;
   begin
      sodium_bin2hex( result'Address , size_t(result'length) ,
                      ptr , size_t(len) ) ;
      return To_Ada(result) ;
   end bin2hex ;
    
   function sodium_hex2bin
     (bin : Address ;
      bin_maxlen : size_t;
      hex : Address ;
      hex_len : size_t;
      ignore : Interfaces.C.Strings.chars_ptr ;
      bin_len : access size_t;
      hex_end : System.Address) return int  -- ../include/sodium/utils.h:65
   with Import => True,
        Convention => C,
     External_Name => "sodium_hex2bin";
   
   function hex2bin(hex : string) return Block_Ptr is
      result : block_ptr := new block_type(1..hex'length/2) ;
      status : int ;
      bin_len : aliased size_t ;
   begin
      status := sodium_hex2bin( result'address , size_t(result'length) ,
                                hex'address , size_t(hex'length) ,
                                interfaces.C.Strings.Null_Ptr , bin_len'access ,
                                System.Null_Address ) ;
      if status = 0
      then
         return result ;
      end if ;
      raise PROGRAM_ERROR ;
   end hex2bin ;
   
   function sodium_bin2base64
     (b64 : Address ;
      b64_maxlen : size_t;
      bin : Address ;
      bin_len : size_t;
      variant : int) return Interfaces.C.Strings.chars_ptr  -- ../include/sodium/utils.h:89
   with Import => True,
        Convention => C,
     External_Name => "sodium_bin2base64";
   	function sodium_base64_encoded_len (bin_len : size_t; variant : int) return size_t  -- ../include/sodium/utils.h:86
   with Import => True, 
        Convention => C, 
     External_Name => "sodium_base64_encoded_len";
   
   function bin2base64( bin : Address ; bin_len : size_t ;
                        variant : integer := BASE64_VARIANT_ORIGINAL ) 
                       return String is
      b64len : size_t ;
   begin
      b64len := sodium_base64_encoded_len(bin_len,int(variant)) ;
      declare
         result : char_array(1..b64len) ;
         status : Interfaces.C.Strings.chars_ptr ;
      begin
         status := sodium_bin2base64(result'address , 
                                     b64len , 
                                     bin , 
                                     bin_len , 
                                     int(variant) ) ;
         return To_Ada(result) ;
      end ;
   end bin2base64 ;
   function sodium_base642bin
     (bin : Address ;
      bin_maxlen : size_t;
      b64 : Address ;
      b64_len : size_t;
      ignore : Interfaces.C.Strings.chars_ptr;
      bin_len : access size_t;
      b64_end : System.Address;
      variant : int) return int  -- ../include/sodium/utils.h:94
   with Import => True,
        Convention => C,
     External_Name => "sodium_base642bin";
   function base642bin( b64 : string ;
                        variant : integer := BASE64_VARIANT_ORIGINAL )
                       return Block_Ptr is
      binmax : block_type(1.. 3 * ((b64'length+4)/4) ) ;
      binlen : aliased size_t ;
      result : block_ptr ;
      status : int ;
   begin
      status := sodium_base642bin( binmax'address ,
                                   binmax'length ,
                                   b64'address ,
                                   b64'length ,
                                   Interfaces.C.Strings.Null_Ptr ,
                                   binlen'access ,
                                   System.Null_Address ,
                                   int(variant) );
      
      result := new Block_Type(1..integer(binlen));
      result(1..integer(binlen)) := binmax(1..integer(binlen)) ;
      return result ;
   end base642bin ;
   
  procedure ShowHex( b : Block_Ptr ) is
  begin
      Put( bin2hex(b.all'Address,b.all'Length) ) ;
  end ShowHex ;

  procedure ShowB64( b : Block_Ptr ) is
  begin
     Put(bin2base64(b.all'Address,b.all'Length)) ;
  end ShowB64 ;
   procedure Set( block : Block_Ptr ; value : String; padding : Character := ASCII.BS ) is
      result : String( 1..block.all'Length) ;
      for result'Address use block.all'Address ;
   begin
      Put("Copying "); Put(value); Put( " to string of length "); Put(result'length); New_Line;
      Ada.Strings.Fixed.Move(value,result, pad => padding) ;
   end Set ;

   function Create(value : String) return Block_Ptr is
      result : Block_Ptr ;
   begin
      result := Create(size_t(value'Length)) ;
      Set(result , value );
      return result ;
   end Create ;

   function Create(value : Ada.Streams.Stream_Element_Array) return Block_Ptr is
      result : Block_Ptr ;
   begin
      result := Create( size_t(value'Length)) ;
      declare
         resbytes : Ada.Streams.Stream_Element_Array(value'range) ;
         for resbytes'Address use result.all'Address ;
      begin
         resbytes := value ;
      end ;
      return result ;
   end Create ;
  procedure Write( str : in out Ada.Streams.Stream_Io.File_Type ; b : Block_Ptr ) is
     blkbytes : Ada.Streams.Stream_Element_Array(1..Ada.Streams.Stream_Element_Offset(b.all'Length)) ;
     for blkbytes'Address use b.all(0)'Address ;
   begin
     Ada.Streams.Stream_Io.Write( str , blkbytes );
   end Write ;

  procedure Read ( str : in out Ada.Streams.Stream_Io.File_Type ; len : Integer ; b : out Block_Ptr) is
     buf : Ada.Streams.Stream_Element_Array(1..Ada.Streams.Stream_Element_Offset(len)) ;
     buflen : Ada.Streams.Stream_Element_Offset ;
  begin
     Ada.Streams.Stream_Io.Read(str,buf,buflen) ;
     b := Create(buf(1..buflen));
   end Read ;
end sodium.helpers;

with System ; use System ;
with Interfaces.C ; use Interfaces.C ;
with sodium.helpers ;

package body sodium.kx is

   function crypto_kx_publickeybytes return size_t  -- ../include/sodium/crypto_kx.h:17
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kx_publickeybytes";

   function crypto_kx_secretkeybytes return size_t  -- ../include/sodium/crypto_kx.h:21
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kx_secretkeybytes";

   function crypto_kx_seedbytes return size_t  -- ../include/sodium/crypto_kx.h:25
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kx_seedbytes";

   function crypto_kx_sessionkeybytes return size_t  -- ../include/sodium/crypto_kx.h:29
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kx_sessionkeybytes";

   function Create return Public_KeyPtr is
      result : Public_KeyPtr ;
   begin
      result := new Block_Type(1..integer(crypto_kx_publickeybytes));
      return result ;
   end Create ;
   function Value( b64str : String ) return Public_KeyPtr is
      result : Block_Ptr ;
   begin
      result := sodium.helpers.base642bin(b64str);
      return Public_KeyPtr(result) ;
   end Value ;
   
   
   function Image( key : Public_KeyPtr ) return String is
   begin
      return sodium.helpers.bin2base64( key.all'address , key'length ) ;
   end Image ;
   
   function Create return Secret_KeyPtr is
      result : Secret_KeyPtr ;
   begin
      result := new Block_Type( 1..integer(crypto_kx_secretkeybytes));
      return result ;
   end Create ;
   function Value( b64str : String ) return Secret_KeyPtr is
      result : Block_Ptr ;
   begin
      result := sodium.helpers.base642bin(b64str);
      return Secret_KeyPtr(result) ;
   end Value ;
   

   function Image( key : Secret_KeyPtr ) return String is
   begin
      return sodium.helpers.bin2base64( key.all'address , key'length ) ;
   end Image ;
   
   function Create (init : Unsigned_8 := 16#1f# ) return Seed_Ptr is
      result : Seed_Ptr ;
   begin
      result := new Block_Type( 1 .. integer (crypto_kx_seedbytes) ) ;
      result.all := (others => init) ;
      return result ;
   end Create ;
   function Create return Session_KeyPtr is
      result : Session_KeyPtr ;
   begin
      result := new Block_Type( 1..integer(crypto_kx_sessionkeybytes)) ;
      return result ;
   end Create ;
   
   function crypto_kx_keypair (pk : Address ; sk : Address ) return int  -- ../include/sodium/crypto_kx.h:42
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_kx_keypair";
   
   function crypto_kx_seed_keypair
     (pk : Address ;
      sk : Address ;
      seed : Address ) return int  -- ../include/sodium/crypto_kx.h:36
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_kx_seed_keypair";
   
   procedure Generate( pk : out Public_KeyPtr ; sk : out Secret_KeyPtr ) is
      status : int ;
   begin
      pk := Create ;
      sk := Create ;
      status := crypto_kx_keypair( pk.all'Address , sk.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "Generate" ;
      end if ;
   end Generate ;

   procedure Generate( pk : out Public_KeyPtr ; sk : out Secret_KeyPtr ;
                       seed : Seed_Ptr ) is
      
      status : int ;
   begin
      pk := Create ;
      sk := Create ;
      status := crypto_kx_seed_keypair( pk.all'Address , sk.all'Address , seed.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "Generate deterministic" ;
      end if ;
   end Generate;
   
   function crypto_kx_client_session_keys
     (rx : Address ;
      tx : Address ;
      client_pk : Address ;
      client_sk : Address ;
      server_pk : Address ) return int  -- ../include/sodium/crypto_kx.h:47
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_kx_client_session_keys";
   
   procedure Generate( rx : out Session_KeyPtr ;
                       tx : out Session_KeyPtr ;
                       client_pk : Public_KeyPtr ;
                       client_sk : Secret_KeyPtr ;
                       server_pk : Public_KeyPtr ) is
      status : int ;
   begin
      rx := Create ;
      tx := Create ;
      status := crypto_kx_client_session_keys( rx.all'Address ,
                                               tx.all'Address ,
                                               client_pk.all'Address ,
                                               client_sk.all'Address ,
                                               server_pk.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "Generate client_session_keys" ;
      end if ;
                                               
   end Generate ;
   function crypto_kx_server_session_keys
     (rx : Address ;
      tx : Address ;
      server_pk : Address ;
      server_sk : Address ;
      client_pk : Address ) return int  -- ../include/sodium/crypto_kx.h:55
   with Import => True, 
        Convention => C, 
     External_Name => "crypto_kx_server_session_keys";
   
   procedure GenerateS( rx : out Session_KeyPtr ;
                        tx : out Session_KeyPtr ;
                        server_pk : Public_KeyPtr ;
                        server_sk : Secret_KeyPtr ;
                        client_pk : Public_KeyPtr ) is
      status : int ;
   begin
      rx := Create ;
      tx := Create ;
      status := crypto_kx_server_session_keys( rx.all'Address ,
                                               tx.all'Address ,
                                               server_pk.all'Address ,
                                               server_sk.all'Address ,
                                               client_pk.all'Address ) ;
      if status /= 0
      then
         raise Program_Error with "Generate server_session_keys" ;
      end if ;               
   end GenerateS ;
   
end sodium.kx;

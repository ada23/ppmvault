-------------------------------------------------------------------------------
-- Secret Key Cryptography
-- Secret Streams and File Encryption
-- https://doc.libsodium.org/secret-key_cryptography/secretstream
-------------------------------------------------------------------------------

with System ; use System ;
with Ada.Streams.Stream_Io ;
with sodium.pwdhash ;
package sodium.sks is

    type KeyPtr_Type is new Block_Ptr ;
    type HeaderPtr_Type is new Block_Ptr ;
    type State_Type is new Block_Ptr ;
    function Generate return KeyPtr_Type ;
    function Generate(pwd : String) return KeyPtr_Type ;    
    function Generate(pwd : String; salt : in out sodium.pwdhash.SaltPtr_type) return KeyPtr_Type ; 
    procedure Save(key : KeyPtr_Type; filename: String) ;
    procedure Load(filename: String ; key : out KeyPtr_Type) ;
    function Create return State_Type ;
    function Create return HeaderPtr_Type ;
    procedure Show ;
    procedure Show( key : KeyPtr_Type );
    procedure Show( header : HeaderPtr_Type ) ;
--
--  Encrypted File Structure
--
--  +------------------------------------------+
--  |                   Header                 |
--  +------------------------------------------+
--  |                                          |
--  |       +-----------------------+          |
--  |       |     Block #1 Length   |          |
--  |       +-----------------------+          |
--  |       |   Block #1 Encrypted  |          |
--  |       +-----------------------+          |
--  |                                          |
--  |       +-----------------------+          |
--  |       |     Block #2 Length   |          |
--  |       +-----------------------+          |
--  |       |   Block #2 Encrypted  |          |
--  |       +-----------------------+          |
--  |                                          |
--  +------------------------------------------+
--

    type Mode_Type is
       (Inp_Stream , Out_Stream) ;

    type Session_Type is new State_Type ;
    procedure Create( str : out Session_Type ; pwd : String ; hdr : in out HeaderPtr_Type ; mode : mode_type ) ;
    -- procedure Create( str : out Session_Type ; pwd : String ; hdr : in out HeaderPtr_Type ; salt : in out sodium.pwdhash.SaltPtr_type; mode : mode_type ) ;
    procedure Write(  str : in out Session_Type ; blockptr : Address; blocklen : Integer ; outbytes : out Block_Ptr; last : boolean := false );
    procedure Read(   str : in out Session_Type ; blockptr : Address; blocklen : Integer ; outbytes : out Block_Ptr ; last : out boolean) ;
    procedure Close(  str : in out Session_Type);

    procedure Encrypt( filein : string ; fileout : string ; pwd : string );
    procedure Decrypt( filein : string ; fileout : string ; pwd : string );
   
end sodium.sks ;

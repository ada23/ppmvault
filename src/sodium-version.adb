with Interfaces.C ; use Interfaces.C ;
with Interfaces.C.strings ; use Interfaces.C.strings ;
with Ada.Strings.Fixed ;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Ada.Unchecked_Deallocation ;
with Ada.Streams.Stream_Io ;

package body sodium.version is

   function SString return String is
   begin
      return Value(sstring) ;
   end SString ;


end sodium.version ;

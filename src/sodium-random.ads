with System; use System ;
with Interfaces ; use Interfaces ;
with Interfaces.C ; use Interfaces.C ;

package sodium.random is

   type Seed_Ptr is new Block_Ptr ;
   function random return Unsigned_32  -- ../include/sodium/randombytes.h:44
   with Import => True, 
        Convention => C, 
     External_Name => "randombytes_random";
   
   function uniform (upper_bound : Unsigned_32) return Unsigned_32  -- ../include/sodium/randombytes.h:47
   with Import => True, 
        Convention => C, 
     External_Name => "randombytes_uniform";
   
  function Seed (fill : Unsigned_8 := 16#1f# ) return Seed_Ptr ;
   
   procedure buf (buf : Address; size : size_t)  -- ../include/sodium/randombytes.h:35
   with Import => True, 
        Convention => C, 
        External_Name => "randombytes_buf";

   procedure buf_deterministic
     (buf : Address;
      size : size_t;
      seed : Address )  -- ../include/sodium/randombytes.h:39
   with Import => True, 
        Convention => C, 
     External_Name => "randombytes_buf_deterministic";
   
end sodium.random;

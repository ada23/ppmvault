with Ada.Command_Line; use Ada.Command_Line ;
with sodium.sks ;

procedure sodecrypt is
   pwd : String := Argument(1) ;
   inpfile : String := Argument(2) ;
   outfile : String := inpfile & ".dec" ;
begin
   sodium.sks.Decrypt(inpfile,outfile,pwd);
end sodecrypt; 

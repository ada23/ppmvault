pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package arm_signal_h is

   subtype sig_atomic_t is int;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/arm/signal.h:17

end arm_signal_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");

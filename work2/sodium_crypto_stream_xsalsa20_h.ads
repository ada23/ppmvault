pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with stddef_h;
with Interfaces.C.Extensions;
with utypes_uuint64_t_h;

package sodium_crypto_stream_xsalsa20_h is

   crypto_stream_xsalsa20_KEYBYTES : constant := 32;  --  /usr/local/include/sodium/crypto_stream_xsalsa20.h:23

   crypto_stream_xsalsa20_NONCEBYTES : constant := 24;  --  /usr/local/include/sodium/crypto_stream_xsalsa20.h:27
   --  unsupported macro: crypto_stream_xsalsa20_MESSAGEBYTES_MAX SODIUM_SIZE_MAX

   function crypto_stream_xsalsa20_keybytes return stddef_h.size_t  -- /usr/local/include/sodium/crypto_stream_xsalsa20.h:25
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_stream_xsalsa20_keybytes";

   function crypto_stream_xsalsa20_noncebytes return stddef_h.size_t  -- /usr/local/include/sodium/crypto_stream_xsalsa20.h:29
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_stream_xsalsa20_noncebytes";

   function crypto_stream_xsalsa20_messagebytes_max return stddef_h.size_t  -- /usr/local/include/sodium/crypto_stream_xsalsa20.h:33
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_stream_xsalsa20_messagebytes_max";

   function crypto_stream_xsalsa20
     (c : access unsigned_char;
      clen : Extensions.unsigned_long_long;
      n : access unsigned_char;
      k : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_stream_xsalsa20.h:36
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_stream_xsalsa20";

   function crypto_stream_xsalsa20_xor
     (c : access unsigned_char;
      m : access unsigned_char;
      mlen : Extensions.unsigned_long_long;
      n : access unsigned_char;
      k : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_stream_xsalsa20.h:41
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_stream_xsalsa20_xor";

   function crypto_stream_xsalsa20_xor_ic
     (c : access unsigned_char;
      m : access unsigned_char;
      mlen : Extensions.unsigned_long_long;
      n : access unsigned_char;
      ic : utypes_uuint64_t_h.uint64_t;
      k : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_stream_xsalsa20.h:47
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_stream_xsalsa20_xor_ic";

   procedure crypto_stream_xsalsa20_keygen (k : access unsigned_char)  -- /usr/local/include/sodium/crypto_stream_xsalsa20.h:54
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_stream_xsalsa20_keygen";

end sodium_crypto_stream_xsalsa20_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");

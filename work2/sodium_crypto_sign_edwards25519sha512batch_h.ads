pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;

package sodium_crypto_sign_edwards25519sha512batch_h is

   crypto_sign_edwards25519sha512batch_BYTES : constant := 64;  --  /usr/local/include/sodium/crypto_sign_edwards25519sha512batch.h:25
   crypto_sign_edwards25519sha512batch_PUBLICKEYBYTES : constant := 32;  --  /usr/local/include/sodium/crypto_sign_edwards25519sha512batch.h:26
   crypto_sign_edwards25519sha512batch_SECRETKEYBYTES : constant := (32 + 32);  --  /usr/local/include/sodium/crypto_sign_edwards25519sha512batch.h:27
   --  unsupported macro: crypto_sign_edwards25519sha512batch_MESSAGEBYTES_MAX (SODIUM_SIZE_MAX - crypto_sign_edwards25519sha512batch_BYTES)

   function crypto_sign_edwards25519sha512batch
     (sm : access unsigned_char;
      smlen_p : access Extensions.unsigned_long_long;
      m : access unsigned_char;
      mlen : Extensions.unsigned_long_long;
      sk : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_sign_edwards25519sha512batch.h:31
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_sign_edwards25519sha512batch";

   function crypto_sign_edwards25519sha512batch_open
     (m : access unsigned_char;
      mlen_p : access Extensions.unsigned_long_long;
      sm : access unsigned_char;
      smlen : Extensions.unsigned_long_long;
      pk : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_sign_edwards25519sha512batch.h:39
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_sign_edwards25519sha512batch_open";

   function crypto_sign_edwards25519sha512batch_keypair (pk : access unsigned_char; sk : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_sign_edwards25519sha512batch.h:47
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_sign_edwards25519sha512batch_keypair";

end sodium_crypto_sign_edwards25519sha512batch_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");

pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;

package sodium_version_h is

   SODIUM_VERSION_STRING : aliased constant String := "1.0.19" & ASCII.NUL;  --  /usr/local/include/sodium/version.h:7

   SODIUM_LIBRARY_VERSION_MAJOR : constant := 26;  --  /usr/local/include/sodium/version.h:9
   SODIUM_LIBRARY_VERSION_MINOR : constant := 1;  --  /usr/local/include/sodium/version.h:10

   function sodium_version_string return Interfaces.C.Strings.chars_ptr  -- /usr/local/include/sodium/version.h:18
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_version_string";

   function sodium_library_version_major return int  -- /usr/local/include/sodium/version.h:21
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_library_version_major";

   function sodium_library_version_minor return int  -- /usr/local/include/sodium/version.h:24
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_library_version_minor";

   function sodium_library_minimal return int  -- /usr/local/include/sodium/version.h:27
   with Import => True, 
        Convention => C, 
        External_Name => "sodium_library_minimal";

end sodium_version_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");

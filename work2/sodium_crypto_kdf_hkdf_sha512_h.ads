pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with stddef_h;
with Interfaces.C.Strings;
with sodium_crypto_auth_hmacsha512_h;

package sodium_crypto_kdf_hkdf_sha512_h is

   --  unsupported macro: crypto_kdf_hkdf_sha512_KEYBYTES crypto_auth_hmacsha512_BYTES
   crypto_kdf_hkdf_sha512_BYTES_MIN : constant := 0;  --  /usr/local/include/sodium/crypto_kdf_hkdf_sha512.h:23
   --  unsupported macro: crypto_kdf_hkdf_sha512_BYTES_MAX (0xff * crypto_auth_hmacsha512_BYTES)

   function crypto_kdf_hkdf_sha512_keybytes return stddef_h.size_t  -- /usr/local/include/sodium/crypto_kdf_hkdf_sha512.h:21
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_hkdf_sha512_keybytes";

   function crypto_kdf_hkdf_sha512_bytes_min return stddef_h.size_t  -- /usr/local/include/sodium/crypto_kdf_hkdf_sha512.h:25
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_hkdf_sha512_bytes_min";

   function crypto_kdf_hkdf_sha512_bytes_max return stddef_h.size_t  -- /usr/local/include/sodium/crypto_kdf_hkdf_sha512.h:29
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_hkdf_sha512_bytes_max";

   function crypto_kdf_hkdf_sha512_extract
     (prk : access unsigned_char;
      salt : access unsigned_char;
      salt_len : stddef_h.size_t;
      ikm : access unsigned_char;
      ikm_len : stddef_h.size_t) return int  -- /usr/local/include/sodium/crypto_kdf_hkdf_sha512.h:32
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_hkdf_sha512_extract";

   procedure crypto_kdf_hkdf_sha512_keygen (prk : access unsigned_char)  -- /usr/local/include/sodium/crypto_kdf_hkdf_sha512.h:38
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_hkdf_sha512_keygen";

   function crypto_kdf_hkdf_sha512_expand
     (c_out : access unsigned_char;
      out_len : stddef_h.size_t;
      ctx : Interfaces.C.Strings.chars_ptr;
      ctx_len : stddef_h.size_t;
      prk : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_kdf_hkdf_sha512.h:42
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_hkdf_sha512_expand";

   type crypto_kdf_hkdf_sha512_state is record
      st : aliased sodium_crypto_auth_hmacsha512_h.crypto_auth_hmacsha512_state;  -- /usr/local/include/sodium/crypto_kdf_hkdf_sha512.h:50
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/include/sodium/crypto_kdf_hkdf_sha512.h:49

   function crypto_kdf_hkdf_sha512_statebytes return stddef_h.size_t  -- /usr/local/include/sodium/crypto_kdf_hkdf_sha512.h:54
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_hkdf_sha512_statebytes";

   function crypto_kdf_hkdf_sha512_extract_init
     (state : access crypto_kdf_hkdf_sha512_state;
      salt : access unsigned_char;
      salt_len : stddef_h.size_t) return int  -- /usr/local/include/sodium/crypto_kdf_hkdf_sha512.h:57
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_hkdf_sha512_extract_init";

   function crypto_kdf_hkdf_sha512_extract_update
     (state : access crypto_kdf_hkdf_sha512_state;
      ikm : access unsigned_char;
      ikm_len : stddef_h.size_t) return int  -- /usr/local/include/sodium/crypto_kdf_hkdf_sha512.h:62
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_hkdf_sha512_extract_update";

   function crypto_kdf_hkdf_sha512_extract_final (state : access crypto_kdf_hkdf_sha512_state; prk : access unsigned_char) return int  -- /usr/local/include/sodium/crypto_kdf_hkdf_sha512.h:67
   with Import => True, 
        Convention => C, 
        External_Name => "crypto_kdf_hkdf_sha512_extract_final";

end sodium_crypto_kdf_hkdf_sha512_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");

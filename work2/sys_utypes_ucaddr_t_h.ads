pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;

package sys_utypes_ucaddr_t_h is

   type caddr_t is new Interfaces.C.Strings.chars_ptr;  -- /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include/sys/_types/_caddr_t.h:30

end sys_utypes_ucaddr_t_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");

pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package sodium_export_h is

   --  unsupported macro: SODIUM_EXPORT __attribute__ ((visibility ("default")))
   --  unsupported macro: SODIUM_EXPORT_WEAK SODIUM_EXPORT
   --  arg-macro: procedure CRYPTO_ALIGN (x)
   --    __attribute__ ((aligned(x)))
   --  arg-macro: function SODIUM_MIN (A, B)
   --    return (A) < (B) ? (A) : (B);
   --  unsupported macro: SODIUM_SIZE_MAX SODIUM_MIN(UINT64_MAX, SIZE_MAX)
end sodium_export_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
